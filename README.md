# @4geit/ngx-dashboard-module [![npm version](//badge.fury.io/js/@4geit%2Fngx-dashboard-module.svg)](//badge.fury.io/js/@4geit%2Fngx-dashboard-module)

---

this module setups a dashboard layout

## Installation

1. A recommended way to install ***@4geit/ngx-dashboard-module*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-dashboard-module) package manager using the following command:

```bash
npm i @4geit/ngx-dashboard-module --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-dashboard-module
```

2. You need to import the `NgxDashboardModule` class in whatever module you want in your project for instance `app.module.ts` as follows:

```js
import { NgxDashboardModule } from '@4geit/ngx-dashboard-module';
```

And you also need to add the `NgxDashboardModule` module within the `@NgModule` decorator as part of the `imports` list:

```js
@NgModule({
  // ...
  imports: [
    // ...
    NgxDashboardModule,
    // ...
  ],
  // ...
})
export class AppModule { }
```
